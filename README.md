# Basic dockerized stack (PHP7-FPM - NGINX - MySQL)

A simple stack for web-apps, fully dockerized, runs with
[docker](https://docs.docker.com/get-started/) 
and [docker-compose](https://docs.docker.com/compose/) (1.7 or higher).


* [Prerequisites](#pre)
* [Installation](#installation)
* [Usage](#usage)
* [How it works?](#explanation)
* [Useful commands](#useful-commands)
* [Customize](#custom)

## Prerequisites:

1. install git:
- https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

1. install Docker:
- https://docs.docker.com/install/#supported-platforms
```
only on Ubuntu: (run docker commands without sudo)
 - Create the docker group. `sudo groupadd docker`
 - Add your user to the docker group. `sudo usermod -aG docker $USER`
 - Log out and log back in so that your group membership is re-evaluated.
 - Verify that you can run docker commands without sudo.
```

1. install Docker Compose:
- https://docs.docker.com/compose/install/

## Installation:
update your system host file and add ```local.project```

download the config files or clone this whole repo.

create an ```.env``` file based on the ```.env.dist``` file. 
Adapt it according to your needs.

## Usage:

```docker-compose build```
- build the necessary containers from scratch locally
may take a while, especially php with all the extra libraries and extensions
only needs to be run once, unless you modify the configs / Dockerfiles

```docker-compose up --remove-orphans```
- bring the project up
you should now be able to visit [local.project](http://local.project/) and see your project
or [local.project:8080](http://local.project:8080) and use phpmyadmin

```docker-compose ps```
- display containers status

```docker-compose exec <container> <command>```
- issue a command inside a container
e.g. ```docker-compose exec nginx sh``` to access the console (inside the nginx container)
and continue issuing commands or ```docker-compose exec php bin/magento c:f``` 
to execute Magento script in php contaner and flush Magento cache

```docker-compose restart```
- restart containers

```docker-compose down --remove-orphans```
- bring the project down (stop and remove containers - keeps images to bring the project
up faster next time)

```docker rm $(docker ps -aq)```
- delete all containers

```docker rmi $(docker images -q)```
- delete all images

## How it works?

Have a look at the `docker-compose.yml` file, it contains all the instructions for
building the the following ```docker-compose``` images:

* `mysql`: This is the MySQL database container.
* `php`: This is the PHP-FPM container in which the application volume is mounted, it also contains composer and xdebug.
* `nginx`: This is the Nginx webserver container in which application volume is mounted too.
* `phpmyadmin`: This is the phpmyadmin container, which is linked to the MySQL database container,
and gives you easy access to the database right from your browser.

Run together, with ```docker-compose up``` they look like this:

```bash
$ docker-compose ps

CONTAINER ID   IMAGE                   COMMAND                  CREATED           STATUS          PORTS                                      NAMES
4e58b8b566d1   phpmyadmin/phpmyadmin   "/run.sh supervisord…"   30 seconds ago    Up 27 seconds   9000/tcp, 0.0.0.0:8080->80/tcp             phpmyadmin
4b6a525fd92b   docker_php              "docker-php-entrypoi…"   31 seconds ago    Up 29 seconds   9000/tcp                                   php
fe527eed73e8   nginx:latest            "nginx -g 'daemon of…"   31 seconds ago    Up 28 seconds   0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   nginx
812d4ab2005c   mysql                   "docker-entrypoint.s…"   31 seconds ago    Up 30 seconds   0.0.0.0:3306->3306/tcp, 33060/tcp          mysql
```

## Customize

If you want to add optional containers like Redis, Elk(Kibana) etc. 
take a look at [docs/custom.md](docs/custom.md).