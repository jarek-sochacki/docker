FROM php:7.0-fpm

WORKDIR /var/www

ENV PHP_EXTRA_CONFIGURE_ARGS --enable-fpm --enable-intl --enable-opcache --enable-zip

RUN apt-get update

RUN apt-get install -y \
    libcurl4-gnutls-dev \
    libxml2-dev \
    libssl-dev

RUN docker-php-ext-install \
    dom \
    pcntl \
    phar \
    posix

RUN apt-get install -y \
    g++ \
    autoconf \
    libbz2-dev \
    libltdl-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libxpm-dev \
    libimlib2-dev \
    libicu-dev \
    libmcrypt-dev \
    libxslt1-dev \
    re2c \
    libvpx-dev \
    zlib1g-dev \
    libgd-dev \
    libtidy-dev \
    libmagic-dev \
    libexif-dev \
    file \
    libssh2-1-dev \
    libjpeg-dev \
    git \
    curl \
    wget \
    librabbitmq-dev \
    libzip-dev

RUN docker-php-ext-install \
    pdo \
    sockets \
    pdo_mysql \
    mysqli \
    mbstring \
    mcrypt \
    hash \
    simplexml \
    xsl \
    soap \
    intl \
    bcmath \
    json \
    opcache \
    zip

RUN pecl install xdebug-2.5.0 && \
    docker-php-ext-enable xdebug

RUN docker-php-ext-configure gd --with-freetype-dir=/usr --with-jpeg-dir=/usr --with-png-dir=/usr && \
    docker-php-ext-install gd

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN chown -R :www-data /var/www && \
    chmod u+x /var/www/


