# How to customise this stack

* [Add Redis](#redis)
* [Add Elk](#elk)
* [Add anything you like](#other)

## Add Redis

1. Update docker-compose.yml file and add the following lines:

    ```yml
    service:
        # ...
        redis:
            image: redis:alpine
            ports:
                - 6379:6379
    ```

Access to redis-cli with:

```bash
# Redis commands
$ docker-compose exec redis redis-cli
```

## Add Elk

1. Create logstash and patterns files locally (e.g. in `./configs/elk/`)

Enter your definitions.

1. Update docker-compose.yml file and add the following lines:

    ```yml
    elk:
        image: willdurand/elk
        ports:
            - 81:80
        volumes_from:
            - php
            - nginx
        volumes:
        ...
    ```
    *(mount the local logstash and pattern files
under `:/etc/logstash` and `:/opt/logstash/patterns` respectively)
    
1. Visit: [local.project:81](http://local.project:81)

## Add anything you like

1. Add via image from public repository:

- find the tool you need
- find a docker image repository (you can start by looking [here](https://github.com/docker-library) or just [googling](http://lmgtfy.com/?q=get+a+new+docker+image) what you need)
- modify the docker-compose.yml file and add the following lines:

     ```yml
        <name>: # name this service, any way you want, use this name in docker-compose commands like `exec` later
            image: <repository/image:tags> # for official docker library omit the repository part, tags are for versioning mainly
            ports:
                - X:Y # Map port Y in the container to port X on the host (they don't have to match)
            volumes: # mount any volumes you want your container to have access to
            volumes_from: # mount the same volumes as another service
                - <service> #that service name (e.g. php)
            ...
    ```
    *(for more advanced options go to the [reference guide](https://docs.docker.com/compose/compose-file/))

1. Add with your own Dockerfile:

- make a Dockerfile:
    - has to be named `Dockerfile`
    - no extension
    
    ```Dockerfile
    FROM <image> # (repository/image:tags)
    
    WORKDIR <path> # path inside the created container - sets the working directory for any instructions that follow 
    
    ENV <key>=<value> # sets the environment variable <key> to the value <value>
    
    RUN <command> # e.g. `apt-get install`, `docker-php-ext-enable`
    
    CMD ["executable","param1","param2"] # provide defaults for an executing container. These defaults can include an executable, or they can omit the executable, in which case you must specify an `ENTRYPOINT` instruction as well.
    ```
     *(for more advanced options go to the [reference guide](https://docs.docker.com/engine/reference/builder/))

- modify the docker-compose.yml file and add the following lines:

     ```yml
        <name>: # name this service, any way you want, use this name in docker-compose commands like `exec` later
            build:
                dockerfile: Dockerfile
                context: <path> # path to your Dockerfile - can be relative to the docker-compose.yml file
            ports:
                - X:Y # Map port Y in the container to port X on the host (they don't have to match)
            volumes: # mount any volumes you want your container to have access to
            volumes_from: # mount the same volumes as another service
                - <service> #that service name (e.g. php)
            ...
    ```
    *(for more advanced options go to the [reference guide](https://docs.docker.com/compose/compose-file/))