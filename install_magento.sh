#!/bin/sh
#
# ---------------------------------------------------------------------
# my personal Magento 2.2 install script. Use at own risk.
# ---------------------------------------------------------------------
#

echo '
initialising containers
'
docker-compose build
docker-compose up -d --remove-orphans
echo '
creating empty database (dropping old database if found)
'
docker-compose exec mysql mysql -psecret -e "DROP DATABASE IF EXISTS game321; CREATE DATABASE IF NOT EXISTS game321;"
echo '
installing dependencies with composer
'
docker-compose exec php composer update
docker-compose exec php composer install -v
echo '
adjusting permissions
'
docker-compose exec php chmod -R 777 var generated vendor pub bin/magento app/etc
docker-compose exec php chown -R :www-data .
echo '
running setup
'
docker exec -e XDEBUG_CONFIG="idekey=PHPSTORM" -e PHP_IDE_CONFIG="serverName=local.project" -it php bin/magento setup:install --base-url=http://local.project/ --backend-frontname=backend --db-host=mysql --db-name=game321 --db-user=user --db-password=userpass --admin-firstname=Ad --admin-lastname=Min --admin-email=admin@admin.com --admin-user=admin --admin-password=admin,1234 --language=en_CA --currency=CAD --timezone=Europe/Warsaw --use-rewrites=1 --cleanup-database
echo '
setting developer mode
'
docker-compose exec php bin/magento deploy:mode:set developer
echo '
disabling cache
'
docker-compose exec php bin/magento cache:disable
echo '
upgrading project
'
docker exec -e XDEBUG_CONFIG="idekey=PHPSTORM" -e PHP_IDE_CONFIG="serverName=local.project" -it php bin/magento setup:upgrade
docker exec -e XDEBUG_CONFIG="idekey=PHPSTORM" -e PHP_IDE_CONFIG="serverName=local.project" -it php bin/magento setup:di:compile
echo '
deploying static content
'
docker exec -e XDEBUG_CONFIG="idekey=PHPSTORM" -e PHP_IDE_CONFIG="serverName=local.project" -it php bin/magento setup:static-content:deploy -f
echo '
reindexing data
'
docker exec -e XDEBUG_CONFIG="idekey=PHPSTORM" -e PHP_IDE_CONFIG="serverName=local.project" -it php bin/magento indexer:reindex
echo '
adjusting permissions on newly generated content, again...
'
docker-compose exec php chmod -R 777 .
echo '
changing backend security settings for ease of access
'
docker-compose exec mysql mysql -psecret game321 -e "INSERT INTO core_config_data(path, value)	VALUES 	('admin/security/admin_account_sharing', '1'),	('admin/security/use_case_sensitive_login', '1'),	('admin/security/use_form_key', '0'),	('admin/security/session_lifetime', '260000'),	('admin/security/password_lifetime', NULL),	('admin/security/password_is_forced', '0');"


